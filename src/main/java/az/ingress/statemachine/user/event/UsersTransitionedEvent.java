package az.ingress.statemachine.user.event;

import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.TransitionServiceImpl;
import az.ingress.statemachine.user.UserStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEvent;
public class UsersTransitionedEvent extends ApplicationEvent {

    private final UserDto userDto;
    private final UserStatus userStatus;



    public UserDto getAccountDto() {
        return userDto;
    }

    public UserStatus getAccountStatus() {
        return userStatus;
    }
    public UsersTransitionedEvent(Object source,UserStatus existingStatus, UserDto dto) {
        super(source);
        this.userDto = dto;
        this.userStatus=existingStatus;
    }

}
