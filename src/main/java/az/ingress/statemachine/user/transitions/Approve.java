package az.ingress.statemachine.user.transitions;


import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.Transition;
import az.ingress.statemachine.user.UserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Approve implements Transition<UserDto> {

    public static final String NAME = "approve";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public UserStatus getTargetStatus() {
        return UserStatus.APPROVED;
    }

    @Override
    public void applyProcessing(UserDto userDto) {
        log.info("user with id {}  is transitioning to {} state ", userDto.getId(),getTargetStatus());
    }
}
