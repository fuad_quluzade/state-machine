package az.ingress.statemachine.user.transitions;

import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.user.Transition;
import az.ingress.statemachine.user.UserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class Submit implements Transition<UserDto> {

    public static final String NAME="submit";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public UserStatus getTargetStatus() {
        return UserStatus.IN_REVIEW;
    }

    @Override
    public void applyProcessing(UserDto userDto) {
        log.info("user with id {}  is transitioning to {} state ", userDto.getId(),getTargetStatus());
         if(userDto.getAge()<18){
             throw new RuntimeException("Age is less than 18");
         }
    }
}
