package az.ingress.statemachine.user;

public interface Transition<T> {
    String getName();

    UserStatus getTargetStatus();

    void applyProcessing(T order);
}
