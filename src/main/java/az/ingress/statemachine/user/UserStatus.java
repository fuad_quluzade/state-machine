package az.ingress.statemachine.user;

import az.ingress.statemachine.user.transitions.Approve;
import az.ingress.statemachine.user.transitions.Notify;
import az.ingress.statemachine.user.transitions.Reject;
import az.ingress.statemachine.user.transitions.Submit;

import java.util.Arrays;
import java.util.List;

public enum UserStatus {
    DRAFT(Submit.NAME),
    IN_REVIEW(Approve.NAME, Reject.NAME),
    APPROVED(Approve.NAME, Notify.NAME),
    NOTIFIED();

    private final List<String> transitions;

    UserStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }
}
