package az.ingress.statemachine.user;

import az.ingress.statemachine.domain.Users;
import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.repository.UserRepository;
import az.ingress.statemachine.user.event.UsersTransitionedEvent;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class TransitionServiceImpl implements TransitionService<UserDto>{

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final ApplicationEventPublisher applicationEventPublisher;
    private Map<String, Transition> transitionsMap;
    private final List<Transition> transitions;

    @Override
    public UserDto transition(Long id, String transitionStr) {
        Transition transition = Optional.ofNullable(transitionsMap.get(transitionStr))
                .orElseThrow(() -> new RuntimeException("Unknown transition " + transitionStr));
        return userRepository.findById(id)
                .map(users -> {
                    checkAllowed(transitionStr,users.getStatus());
                    transition.applyProcessing(modelMapper.map(users,UserDto.class));
                    return updateStatus(users,transition.getTargetStatus());
                })
                .map(u->modelMapper.map(u,UserDto.class))
                .orElseThrow(()->new IllegalArgumentException("No user found with id "+ id));
    }

    private Object updateStatus(Users users, UserStatus targetStatus) {
        UserStatus existingStatus = users.getStatus();
        users.setStatus(targetStatus);
        Users updated = userRepository.save(users);

        var event = new UsersTransitionedEvent(this, existingStatus, modelMapper.map(updated, UserDto.class));
        applicationEventPublisher.publishEvent(event);
        return updated;
    }

    @Override
    public List<String> getAllowedTransitions(Long id) {
        return userRepository.findById(id)
                .orElseThrow(()->new RuntimeException("User not fount id " + id))
                .getStatus().getTransitions();

        }

    @PostConstruct
    private void initTransitions() {
        Map<String, Transition> transitionHashMap = new HashMap<>();
        for (Transition accountTransition : transitions) {
            if (transitionHashMap.containsKey(accountTransition.getName())) {
                throw new IllegalStateException("Duplicate transition :" + accountTransition.getName());
            }
            transitionHashMap.put(accountTransition.getName(), accountTransition);
        }
        this.transitionsMap = transitionHashMap;
    }

    private void checkAllowed(String transition, UserStatus status) {
                   status.getTransitions()
                           .stream()
                           .filter(transition::equals)
                           .findFirst()
                           .orElseThrow(()->new RuntimeException("Transition :" + transition+ " from status : "+
                                   status   +" is not allowed"));
    }

//    private void checkAllowed(Transition accountTransition, UserStatus status) {
//        Set<UserStatus> allowedSourceStatuses = Stream.of(UserStatus.values())
//                .filter(s -> s.getTransitions().contains(accountTransition.getName()))
//                .collect(Collectors.toSet());
//        if (!allowedSourceStatuses.contains(status)) {
//            throw new RuntimeException("The transition from the current state " + status.name() + "  to the target state "
//                    + accountTransition.getTargetStatus().name() + "  is not allowed!");
//        }
//    }


}
