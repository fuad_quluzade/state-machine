package az.ingress.statemachine;

import az.ingress.statemachine.domain.Users;
import az.ingress.statemachine.dto.UserDto;
import az.ingress.statemachine.repository.UserRepository;
import az.ingress.statemachine.user.TransitionService;
import az.ingress.statemachine.user.UserStatus;
import az.ingress.statemachine.user.transitions.Approve;
import az.ingress.statemachine.user.transitions.Notify;
import az.ingress.statemachine.user.transitions.Reject;
import az.ingress.statemachine.user.transitions.Submit;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class StateMachineApplication implements CommandLineRunner {
    private final TransitionService<UserDto> transitionService;
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(StateMachineApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Users users=Users.builder()
                .age(20)
                .firstname("Fuad")
                .lastname("Quluzade")
                .password("12345")
                .status(UserStatus.DRAFT)
                .build();
        userRepository.save(users);
        transitionService.transition(users.getId(), Submit.NAME);
        transitionService.transition(users.getId(), Reject.NAME);
        transitionService.transition(users.getId(), Submit.NAME);
        transitionService.transition(users.getId(), Approve.NAME);
        transitionService.transition(users.getId(), Approve.NAME);
        transitionService.transition(users.getId(), Approve.NAME);
        transitionService.transition(users.getId(), Notify.NAME);

    }
}
